# Reto Servicio Social Backend

`v1.0.1`

Este proyecto es realizado para capacitarme en tecnologías que se utilizaran en futuros proyectos.

<details>
  <summary>Tabla de contenidos</summary>
  <ol>
    <li>
      <a href="#cómo-empezar">Como empezar</a>
      <ul>
        <li><a href="#prerequisitos">Prerequisitos</a></li>
        <li><a href="#instalación">Instalación</a></li>
      </ul>
    </li>
    <li>
      <a href="#uso">Uso</a>
      <ul>
        <li><a href="#verificar-funcionamiento">Verificar funcionamiento</a></li>
        <li><a href="#detener-la-aplicación">Detener la aplicación</a></li>
        <li><a href="#verificar-los-logs-de-los-contenedores">Verificar los logs de los contenedores</a></li>
        <li><a href="#bash-o-sh">Bash o Sh</a></li>
      </ul>
    </li>
    <li><a href="#construido-con">Construido con</a></li>
    <li><a href="#librerías-utilizadas">Librerías utilizadas</a></li>
    <li><a href="#librerías-no-utilizadas">Librerías no utilizadas</a></li>
    <li><a href="#imágenes">Imágenes</a></li>
  </ol>
</details>


## Cómo empezar

Siguiendo las instrucciones adecuadamente podrás realizar la instalación del proyecto sin problema.

**NOTA**: Los comandos a continuación funcionan para [Ubuntu 20.04 LTS](https://releases.ubuntu.com/20.04/) o distros similares.

### Prerequisitos

* [Docker 20.10.17](https://docs.docker.com/engine/install/ubuntu/) o superior
* [Docker-compose 1.25.0](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04) o superior
* [Git](https://git-scm.com/downloads)

### Instalación

1.- **Clona el repositorio**

Al ejecutar este comando te descargará los archivos
en el directorio donde te encuentras, asegúrate de
estar en un directorio correcto. Por ejemplo: `~/Downloads`

```shell
git clone https://gitlab.com/mmedinam1600/reto-servicio-social-backend.git
```

2.- **Dirígete a la carpeta**

Se creará un folder llamado `reto-servicio-social-backend`,
desde la misma terminal ejecuta el siguiente comando:

```shell
cd reto-servicio-social-backend
```

3.- **Crea tus variables de entorno**

En el directorio raíz del repositorio crea un nuevo
archivo `.env` y copia las siguientes variables de
entorno requeridas:

```dotenv
# symfony
APP_ENV=
APP_SECRET=

# docker containers
SERVER_PORT=
DATABASE_PORT=

# mariadb docker config
MARIADB_SET_USER=
MARIADB_SET_PASSWORD=

DATABASE_URL=

JWT_SECRET_KEY=
JWT_PUBLIC_KEY=
JWT_PASSPHRASE=
JWT_TOKEN_TTL=

CORS_ALLOW_ORIGIN=
```

> **NOTA:** El repositorio cuenta con un archivo `.env.example` para la configuración de cada variable.

## Uso

Una vez terminada la [instalación](#instalación),
la aplicación está automatizada para poder correrla
usando el comando:

```shell
sudo docker-compose build && sudo docker-compose up -d
```

> **IMPORTANTE**: Asegúrate de que ejecutes estos comandos
> en el directorio raíz del repositorio.

Para verificar que la aplicación está funcionando
correctamente, ejecuta el siguiente comando:

```shell
sudo docker ps
```

### Verificar funcionamiento

Debera tener registrados los siguientes contenedores:

* reto_ss_backend_nginx
* reto_ss_backend_app
* reto_ss_backend_mariadb

```shell
CONTAINER ID   IMAGE                  COMMAND                  CREATED          STATUS          PORTS                                       NAMES
6ccda4689cb2   nginx:latest           "/docker-entrypoint.…"   16 seconds ago   Up 15 seconds   0.0.0.0:8000->80/tcp, :::8000->80/tcp       reto_ss_backend_nginx
d404be993d9f   reto_ss_backend_app    "docker-php-entrypoi…"   17 seconds ago   Up 16 seconds   9000/tcp                                    reto_ss_backend_app
f3b5bdd834cf   mariadb:10.7.4-focal   "docker-entrypoint.s…"   17 seconds ago   Up 16 seconds   0.0.0.0:8001->3306/tcp, :::8001->3306/tcp   reto_ss_backend_mariadb
```

### Poblar base con datos prueba

Para poblar la base de datos es necesario que entre
a la consola del servidor php para ejecutar un
comando de symfony.

Para ello, ejecute el siguiente comando:

```shell
sudo docker exex -it reto_ss_backend_app bash
```

Y seguidamente en la consola ejecutar el siguiente comando:

```shell
symfony console doctrine:fixtures:load
```

Le pedirá una confirmación de que la base de datos
se alterara, ponga `yes`.

### Listo

Ya que verifico que los 3 contenedores están funcionando,
ya puede ingresar a la página desde el navegador ingresando:

`http://localhost:8000`

> **Nota**: Recuerde remplazar el puerto por el que puso en la
> variable de entorno `SERVER_PORT`

### Funcionamiento de la API

[Vease apartado de API](api.md)


### Detener la aplicación

Para detener la aplicación utilice el siguiente comando
en el directorio raiz del proyecto (Donde se encuentran los
archivos Dockerfile y docker-compose.yml).

```shell
sudo docker-compose down
```

### Verificar los logs de los contenedores

Verificar los logs de los contenedores es muy útil a la
hora de 'debuggear' nuestra aplicación. Ya que si por
alguna razón tenemos errores o simplemente queremos ver
que pasa tras bambalinas del contenedor podermos hacerlo
con el siguiente comando:

```shell
sudo docker logs [nombre-contenedor]
```

> Valores posibles para **nombre-contenedor**:
> * reto_ss_backend_nginx
> * reto_ss_backend_app
> * reto_ss_backend_mariadb


### Bash o Sh

Si queremos continuar con el desarrollo de la aplicación
o seguir 'debugeando' podemos acceder directamente a la
consola del contenedor con el siguiente comando:

```shell
sudo docker exec -it [nombre-contenedor] [bash|sh]
```

> Nota: algunos contenedores no incluyen instalado `bash`,
> por lo que su antecesor es `sh`.

## Construido con:

* php 8.1
* Docker
* composer
* symfony
* nginx
* mariadb

## Librerías utilizadas

* Symfony Routing
> Esta librería es esencial para la aplicación, ya que
> es la encargada de poner manejar nuestras rutas y
> poder responder al cliente mediante una respuesta,
> ya sea el renderizado de una vista o una respuesta json.
>
> En el proyecto fue utilizada para crear nuestra API REST
> y asi mismo para el renderizado de nuestras vistas.

* Doctrine Database Abstraction Layer
> También una de las librerías más utilizadas por su
> versatilidad para manejar query's de bases de datos
> relacionales. Tiene compatibilidad con la mayoría y
> una sintaxis muy sencilla de emplear a comparación
> de raw query's. Además de que también brinda servicios
> de protección ára evitar una query injection.
>
> EL proyecto hace uso de esta librería para crear y
> administrar las entidades necesarias que la aplicación
> requerira, en este caso para la entidad User, Event y Venue.

* Symfony Http Foundation
> Esta librería es un componente para tener una capa
> orientada a objetos para la especificación HTTP.
>
> Es de gran utilidad, ya que fue usada para crear objetos
> Response y JsonResponse, y poder dar una respuesta con
> ciertos parámetros. Asi mismo fue utilizado para crear y
> borrar cookies del lado del cliente para guardar el JWT.

* Twig
> Un motor de vistas como cualquier otro, pero para php.
> Teniendo la capacidad de crear contenido dinámico del
> lado del servidor haciendo server-side-rendering y uso
> de plantillas para hacer código reciclable.
>
> Fue utilizado para crear las interfaces de nuestra
> página web y de gran utilidad para nuestro contenido
> dinámico.

* PHP 8
> Una de las versiones más recientes de PHP donde incluye
> grandes mejoras para el desarrollo.
>
> Se utilizó por su alta fecha estimada de soporte y sus
> nuevas características que incluye.

* jwt-authentication-bundle
> Librería oficial de symfony para autentificar a usuarios
>
> Utilizada para la autentificación del usuario y hacer
> uso de nuestros endpoints que solamente tienen acceso
> ciertos roles en específico.

* form
> Esta librería es de gran utilidad para crear formularios
> basados en entidades o simplemente para facilitar la
> creación de un formulario.
>
> Fue utilizada para crear los formularios de inicio de
> sesión, registro, crear y editar eventos.

* http-client
> Esta librería es para hacer fetch a alguna url en específica
> generalmente son API's las que se consultan.
>
> Se utilizó para poder tener comunicación con nuestra API
> REST y conectar la lógica a nuestras interfaces.

* webpack-encore-bundle
> Muy usada por la mayoría de programadores web para empaquetar
> sus assets y hacer muchas cosas con ellos como generar 1
> solo archivo y optimizarlos.
>
> En el proyecto fue usada para tener todos nuestros assets
> en 1 solo lugar y al ser compilados solo se generará 1 archivo,
> esto es en caso de que se implementen algunos frameworks de css o js.

* nodejs
> Un poderoso entorno de desarrollo de javascript, utilizado
> para muchas cosas de desarrollo web.
>
> Utilizado como dependencia de webpack para poder compilar
> nuestros assets.


## Librerías no utilizadas

* Symfony Http Kernel
> Symfony proporciona un componente HttpKernel que
> sigue el protocolo HTTP: convierte una Solicitud
> en una Respuesta.
>
> Implícitamente en nuestra aplicación si es utilizada,
> ya que Http Kernel se apoya de Http Foundation

* Phinx.org
> Phinx es utilizado para manipular y alterar la base de
> datos de una manera clara y concisa. Es muy similar a
> un ORM.
>
> No se utilizó, ya que el uso de esta librería se suele
> ocupar para proyectos muy grandes y con alto crecimiento.
> Con doctrine se pueden hacer muchas cosas y no fue
> necesario implementarla.

## Imágenes

**Página principal**

![Home page](./github/resources/ss1.png)

**Eventos por categoría**

![Home page](./github/resources/ss2.png)

**Panel de administración**

![Home page](./github/resources/ss3.png)

**Info del evento**

![Home page](./github/resources/ss4.png)

**Inicio de Sesión**

![Home page](./github/resources/ss5.png)

**Registro**

![Home page](./github/resources/ss6.png)

**Crear evento**

![Home page](./github/resources/ss7.png)

**Editar evento**

![Home page](./github/resources/ss8.png)
