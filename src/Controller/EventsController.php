<?php

namespace App\Controller;

use App\Entity\Event;
use App\Repository\EventRepository;
use App\Repository\UserRepository;
use App\Repository\VenueRepository;
use App\Service\ApiResponse;
use App\Service\FileUploader;
use App\utils\VerifyRoles;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Exception\ORMException;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

#[Route('/api')]
class EventsController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private EventRepository $eventRepository;
    private JWTTokenManagerInterface $jwtManager;
    private TokenStorageInterface $tokenStorageInterface;
    private UserRepository $userRepository;
    private VenueRepository $venueRepository;

    public function __construct(
        EventRepository $eventRepository,
        EntityManagerInterface $entityManager,
        UserRepository $userRepository,
        VenueRepository $venueRepository,
        TokenStorageInterface $tokenStorageInterface,
        JWTTokenManagerInterface $jwtManager
    ) {
        $this->eventRepository = $eventRepository;
        $this->entityManager = $entityManager;
        $this->jwtManager = $jwtManager;
        $this->tokenStorageInterface = $tokenStorageInterface;
        $this->userRepository = $userRepository;
        $this->venueRepository = $venueRepository;
    }


    #[Route('/events', name: 'get_events', methods: ['GET'])]
    public function index(Request $res): JsonResponse
    {
        try {
            $page = $res->query->getInt("page", 1);
            $limit = $res->query->getInt("limit", 10);
            $offset = $res->query->getInt("offset", ($page - 1) * ($limit + 1));
            $category = $res->query->getInt("category", 0);

            $qb = $this->entityManager->createQueryBuilder();
            $qb->select('event')
                ->from("App:Event", "event")
                ->where("event.active = true");
            $date = new DateTime('now');
            switch ($category) {
                case 1: //Next events
                    $qb->where('event.scheduledAt >= :date')
                        ->setParameter("date", $date);
                    break;
                case 2: // Old events
                    $qb->where('event.scheduledAt < :date')
                        ->setParameter('date', $date);
                    break;
                case 3: // Discount events
                    $qb->where('event.percentageDiscount > 0');
                    break;
                default: //No categories
                    break;
            }
            $events = $qb
                ->orderBy("event.scheduledAt", "ASC")
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->getQuery()
                ->getResult();

            return new ApiResponse("Listado de eventos", $events, [], 200);
        } catch (\Exception $e) {
            return new ApiResponse(
                "Error al obtener los eventos",
                null,
                array(["message" => $e->getMessage(), "statusCode" => 500]),
                500
            );
        }
    }

    #[Route('/events/{id}', name: 'get_event', methods: ['GET'])]
    public function getEvent($id): JsonResponse
    {
        try {
            $event = $this->eventRepository->findOneBy(["id" => $id, "active" => true], ["scheduledAt" => 'ASC']);
            if (!$event) {
                return new ApiResponse(
                    "El evento con el id {$id} no existe",
                    null,
                    array(["message" => "Error al obtener el evento con el id {$id}", "statusCode" => 400]),
                    400
                );
            }

            return new ApiResponse("Evento obtenido", $event, [], 200);
        } catch (\Exception $e) {
            return new ApiResponse(
                "Error al obtener los eventos",
                null,
                array(["message" => $e->getMessage(), "statusCode" => 500]),
                500
            );
        }
    }

    #[Route('/events', name: 'add_event', methods: ['POST'])]
    public function addEvent(Request $req, FileUploader $fileUploader): JsonResponse
    {
        try {
            $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
            $data = json_decode($req->getContent(), true);
            $banner = $req->files->get('banner');
            $newEvent = new Event();

            if ($banner) {
                $bannerFileName = $fileUploader->upload($banner);
                $newEvent->setBanner('/uploads/images/' . $bannerFileName);
            } else {
                $newEvent->setBanner($data['banner']);
            }

            $user = $this->userRepository->findOneBy(["email" => $decodedJwtToken['username']]);
            $venue = $this->venueRepository->find($data['venueId']);

            $newEvent
                ->setTitle($data['title'])
                ->setDescription($data['description'])
                ->setOrganizer($data['organizer'])
                ->setPresale(new DateTime($data['presale']) ?? null)
                ->setPrice($data['price'])
                ->setPercentageDiscount($data['percentageDiscount'] ?? null)
                ->setScheduledAt(new DateTime($data['scheduledAt']))
                ->setRestrictions($data['restrictions'] ?? null)
                ->setEstimatedDuration($data['estimatedDuration'])
                ->setAgeLimit($data['ageLimit'] ?? null)
                ->setUserId($user)
                ->setVenueId($venue);

            $this->entityManager->persist($newEvent);
            $this->entityManager->flush();
            return new ApiResponse("Evento creado satisfactoriamente", $newEvent, [], 201);
        } catch (JWTDecodeFailureException $e) {
            return new ApiResponse("Error al decodificar el JWT", null, array($e), 401);
        } catch (\Exception $e) {
            return new ApiResponse("Error general", null, array($e->getMessage()), 500);
        }
    }

    #[Route('/events/{id}', name: 'delete_event', methods: ['DELETE'])]
    public function deleteEvent($id): JsonResponse
    {
        try {
            $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());
            if (VerifyRoles::isAdmin($decodedJwtToken)) {
                return new ApiResponse(
                    "Acceso denegado",
                    null,
                    array("message" => "No tienes permiso para borrar un evento."),
                    401
                );
            }

            $event = $this->entityManager->getReference('\App\Entity\Event', $id);
            $this->entityManager->remove($event);
            $this->entityManager->flush();

            return new ApiResponse("Evento con id: {$id} borrado satisfactoriamente", null, [], 200);
        } catch (JWTDecodeFailureException $e) {
            return new ApiResponse("Error al decodificar el JWT", null, array($e->getMessage()), 401);
        } catch (ORMException $e) {
            return new ApiResponse("Error borrar el evento.", null, array($e->getMessage()), 500);
        }
    }

    #[Route('/events/{id}', name: 'update_event', methods: ['PUT'])]
    public function updateEvent(Request $req, $id, FileUploader $fileUploader): JsonResponse
    {
        try {
            // Obtain the user token
            $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());

            // Check if we have the administrator role
            if (VerifyRoles::isAdmin($decodedJwtToken)) {
                return new ApiResponse(
                    "Acceso denegado",
                    null,
                    array("message" => "no tienes permiso para actualizar un evento."),
                    401
                );
            }

            // If everything is OK, we get the data to change from the event
            $data = json_decode($req->getContent(), true);
            $banner = $req->files->get('banner');

            // Search the event
            $event = $this->eventRepository->find($id);
            // If the event does not exist, it responds with an error
            if (!$event) {
                return new ApiResponse(
                    "Evento no encontrado",
                    null,
                    array("El evento con el id: {$id} no se encontró."),
                    400
                );
            }

            // If the image changes we will upload it again.
            if ($banner) {
                $bannerFileName = $fileUploader->upload($banner);
                $event->setBanner('/uploads/images/' . $bannerFileName);
            } else {
                $event->setBanner($data['banner']);
            }

            //search the venue
            $venue = $this->venueRepository->find($data['venueId']);

            // we update the data
            $event
                ->setTitle($data['title'])
                ->setDescription($data['description'])
                ->setOrganizer($data['organizer'])
                ->setPresale(new DateTime($data['presale']) ?? null)
                ->setPrice($data['price'])
                ->setPercentageDiscount($data['percentageDiscount'] ?? null)
                ->setScheduledAt(new DateTime($data['scheduledAt']))
                ->setRestrictions($data['restrictions'] ?? null)
                ->setEstimatedDuration($data['estimatedDuration'])
                ->setAgeLimit($data['ageLimit'] ?? null)
                ->setVenueId($venue);

            $this->entityManager->persist($event);
            $this->entityManager->flush();

            return new ApiResponse("Evento editado satisfactoriamente", $event, [], 201);
        } catch (JWTDecodeFailureException $e) {
            return new ApiResponse("Error al decodificar el JWT", null, array($e->getMessage()), 401);
        } catch (\Exception $e) {
            return new ApiResponse("Error general", null, array($e->getMessage()), 500);
        }
    }
}
