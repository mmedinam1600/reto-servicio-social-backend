<?php


namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\ApiResponse;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;


#[Route('/api')]
class AuthController extends AbstractController
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    #[Route('/register', name: 'auth_register', methods: ['POST'])]
    public function register(
        Request $req,
        UserPasswordHasherInterface $passwordHasher,
        EntityManagerInterface $entityManager
    ): JsonResponse {
        $parameters = json_decode($req->getContent(), true);
        if ($this->userRepository->findOneBy(["email" => $parameters['username']])) {
            return new ApiResponse(
                "Correo electrónico en uso.",
                null,
                array(["message" => "Este correo ya esta registrado.", "statusCode" => 400]),
                400
            );
        }
        $user = new User();
        $hashedPassword = $passwordHasher->hashPassword(
            $user,
            $parameters['password']
        );
        $user->setEmail($parameters['username']);
        $user->setPassword($hashedPassword);
        $user->setFirstName($parameters['firstName']);
        $user->setLastName($parameters['lastName']);
        $user->setCountry($parameters['country']);
        $user->setPostalCode($parameters['postalCode']);
        $user->setRoles(["ROLE_USER"]);

        $entityManager->persist($user);
        $entityManager->flush();

        return new ApiResponse("Usuario creado satisfactoriamente", null, [], 201);
    }
}
