<?php

namespace App\Controller;

use App\Form\EventFormType;
use App\Form\LoginFormType;
use App\Form\RegisterFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class PagesController extends AbstractController
{

    #[Route('/', name: 'index_page')]
    public function index(Request $request): Response
    {
        try {
            $cookies = $request->cookies;
            $token = null;
            if ($cookies->has('token')) {
                $token = $cookies->get('token');
                $token = preg_split('/[.]/', $token);
                $decryptedToken = base64_decode($token[1]);
                $jsonToken = json_decode($decryptedToken);
                $isAdmin = in_array("ROLE_ADMIN", $jsonToken->roles);
            }

            $httpClient = HttpClient::create();
            $responseNext = $httpClient->request(
                'GET',
                'http://webserver/api/events?page=1&limit=5&offset=0&category=1'
            );
            $responseOld = $httpClient->request(
                'GET',
                'http://webserver/api/events?page=1&limit=5&offset=0&category=2'
            );
            $responseOffer = $httpClient->request(
                'GET',
                'http://webserver/api/events?page=1&limit=5&offset=0&category=3'
            );
            return $this->render('pages/index.html.twig', [
                'responseNext' => $responseNext->toArray(),
                'responseOld' => $responseOld->toArray(),
                'responseOffer' => $responseOffer->toArray(),
                'jwt' => $token,
                'isAdmin' => $isAdmin ?? false
            ]);
        } catch (TransportExceptionInterface|ClientExceptionInterface|RedirectionExceptionInterface|ServerExceptionInterface|DecodingExceptionInterface $e) {
            return $this->render('pages/error.html.twig', [
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);
        }
    }

    #[Route('/events', name: 'events_page')]
    public function events(Request $request): Response
    {
        try {
            $cookies = $request->cookies;
            $token = null;
            if ($cookies->has('token')) {
                $token = $cookies->get('token');
                $token = preg_split('/[.]/', $token);
                $decryptedToken = base64_decode($token[1]);
                $jsonToken = json_decode($decryptedToken);
                $isAdmin = in_array("ROLE_ADMIN", $jsonToken->roles);
            }

            $page = $request->get('page') ?? 1;
            $category = $request->get('category') ?? 0;
            $limit = $request->get('limit') ?? 10;
            $offset = $request->get('offset') ?? 0;

            $url = $this->generateUrl(
                'get_events',
                [
                    "page" => $page,
                    "category" => $category,
                ]
            );

            $httpClient = HttpClient::create();
            $response = $httpClient->request(
                'GET',
                "http://webserver" . $url
            );
            $pages = floor(sizeof($response->toArray()) / ($limit + 1)) + 1;

            return $this->render('pages/events.html.twig', [
                'events' => $response->toArray(),
                'pages' => $pages,
                'page' => $page,
                'category' => $category,
                'jwt' => $token,
                'isAdmin' => $isAdmin ?? false
            ]);
        } catch (TransportExceptionInterface|ClientExceptionInterface|DecodingExceptionInterface|RedirectionExceptionInterface|ServerExceptionInterface $e) {
            return $this->render('pages/error.html.twig', [
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);
        }
    }

    #[Route('/event/create/', name: 'create_event_page')]
    public function createEvent(Request $request): Response
    {
        try {
            $cookies = $request->cookies;
            $token = null;
            if ($cookies->has('token')) {
                $token = $cookies->get('token');
                $tokenData = preg_split('/[.]/', $token);
                $decryptedToken = base64_decode($tokenData[1]);
                $jsonToken = json_decode($decryptedToken);
                $isAdmin = in_array("ROLE_ADMIN", $jsonToken->roles);
            }

            $form = $this->createForm(EventFormType::class);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $httpClient = HttpClient::create();
                $eventData = $form->getData();
                $eventData['presale'] = $eventData['presale']->format('Y/m/d H:i:s');
                $eventData['scheduledAt'] = $eventData['scheduledAt']->format('Y/m/d H:i:s');

                $response = $httpClient->request(
                    'POST',
                    'http://webserver/api/events',
                    [
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'Authorization' => 'Bearer ' . $token,
                        ],
                        'body' => json_encode($eventData)
                    ]
                );

                return $this->redirectToRoute('index_page');
            }


            return $this->render('pages/createEvent.html.twig', [
                'jwt' => $token,
                'isAdmin' => $isAdmin ?? false,
                'form' => $form->createView(),
            ]);
        } catch (ClientExceptionInterface|RedirectionExceptionInterface|ServerExceptionInterface $e) {
            return $this->render('pages/error.html.twig', [
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
            ]);
        }
    }

    #[Route('/event/{id}', name: 'event_page')]
    public function event(Request $request, $id): Response
    {
        try {
            $cookies = $request->cookies;
            $token = null;
            if ($cookies->has('token')) {
                $token = $cookies->get('token');
                $token = preg_split('/[.]/', $token);
                $decryptedToken = base64_decode($token[1]);
                $jsonToken = json_decode($decryptedToken);
                $isAdmin = in_array("ROLE_ADMIN", $jsonToken->roles);
            }
            $httpClient = HttpClient::create();
            $response = $httpClient->request(
                'GET',
                'http://webserver/api/events/' . $id
            );
            return $this->render('pages/event.html.twig', [
                'event' => $response->toArray(),
                'jwt' => $token,
                'isAdmin' => $isAdmin ?? false
            ]);
        } catch (TransportExceptionInterface|ClientExceptionInterface|DecodingExceptionInterface|RedirectionExceptionInterface|ServerExceptionInterface $e) {
            return $this->render('pages/error.html.twig', [
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);
        }
    }

    #[Route('/panel', name: 'panel_page')]
    public function panel(Request $request): Response
    {
        try {
            $cookies = $request->cookies;
            $token = null;
            if ($cookies->has('token')) {
                $token = $cookies->get('token');
                $token = preg_split('/[.]/', $token);
                $decryptedToken = base64_decode($token[1]);
                $jsonToken = json_decode($decryptedToken);
                $isAdmin = in_array("ROLE_ADMIN", $jsonToken->roles);
                if (!$isAdmin) {
                    return $this->redirectToRoute('index_page');
                }
            }

            $httpClient = HttpClient::create();
            $responseNext = $httpClient->request(
                'GET',
                'http://webserver/api/events?category=0&limit=15'
            );

            return $this->render('pages/adminPanel.html.twig', [
                'jwt' => $token,
                'isAdmin' => $isAdmin ?? false,
                'events' => $responseNext->toArray()
            ]);
        } catch (TransportExceptionInterface|ClientExceptionInterface|DecodingExceptionInterface|RedirectionExceptionInterface|ServerExceptionInterface $e) {
            return $this->render('pages/error.html.twig', [
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
            ]);
        }
    }

    #[Route('/event/delete/{id}', name: 'delete_event_page')]
    public function deleteEvent(Request $request, $id): Response
    {
        try {
            $cookies = $request->cookies;
            $token = null;
            if ($cookies->has('token')) {
                $token = $cookies->get('token');
                $tokenData = preg_split('/[.]/', $token);
                $decryptedToken = base64_decode($tokenData[1]);
                $jsonToken = json_decode($decryptedToken);
                $isAdmin = in_array("ROLE_ADMIN", $jsonToken->roles);
                if (!$isAdmin) {
                    return $this->redirectToRoute('index_page');
                }
            }

            $httpClient = HttpClient::create();
            $response = $httpClient->request(
                'DELETE',
                'http://webserver/api/events/' . $id,
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $token
                    ]
                ]
            );

            return $this->redirectToRoute('panel_page');
        } catch (TransportExceptionInterface|ClientExceptionInterface|RedirectionExceptionInterface|ServerExceptionInterface $e) {
            return $this->render('pages/error.html.twig', [
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
            ]);
        }
    }

    #[Route('/event/edit/{id}', name: 'edit_event_page')]
    public function editEvent(Request $request, $id): Response
    {
        try {
            $cookies = $request->cookies;
            $token = null;
            if ($cookies->has('token')) {
                $token = $cookies->get('token');
                $tokenData = preg_split('/[.]/', $token);
                $decryptedToken = base64_decode($tokenData[1]);
                $jsonToken = json_decode($decryptedToken);
                $isAdmin = in_array("ROLE_ADMIN", $jsonToken->roles);
                if (!$isAdmin) {
                    return $this->redirectToRoute('index_page');
                }
            }

            $form = $this->createForm(EventFormType::class);
            $form->handleRequest($request);

            $httpClient = HttpClient::create();
            $responseEvent = $httpClient->request(
                'GET',
                'http://webserver/api/events/' . $id
            );

            if ($form->isSubmitted() && $form->isValid()) {
                $eventData = $form->getData();
                $eventData['presale'] = $eventData['presale']->format('Y/m/d H:i:s');
                $eventData['scheduledAt'] = $eventData['scheduledAt']->format('Y/m/d H:i:s');

                $response = $httpClient->request(
                    'PUT',
                    'http://webserver/api/events/' . $id,
                    [
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'Authorization' => 'Bearer ' . $token,
                        ],
                        'body' => json_encode($eventData)
                    ]
                );

                return $this->redirectToRoute('panel_page');
            }

            return $this->render('pages/editEvent.html.twig', [
                'jwt' => $token,
                'isAdmin' => $isAdmin ?? false,
                'form' => $form->createView(),
                'event' => $responseEvent->toArray()
            ]);
        } catch (TransportExceptionInterface|ClientExceptionInterface|RedirectionExceptionInterface|ServerExceptionInterface|DecodingExceptionInterface $e) {
            return $this->render('pages/error.html.twig', [
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
            ]);
        } catch (\Exception $e) {
            return $this->render('pages/error.html.twig', [
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
            ]);
        }
    }

    #[Route('/login', name: 'login_page')]
    public function login(Request $request): Response
    {
        try {
            $cookies = $request->cookies;
            if ($cookies->has('token')) {
                return $this->redirectToRoute('index_page');
            }
            $form = $this->createForm(LoginFormType::class);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $credentials = $form->getData();
                $httpClient = HttpClient::create();
                $response = $httpClient->request(
                    'POST',
                    'http://webserver/api/login',
                    [
                        'headers' => [
                            'Content-Type' => 'application/json'
                        ],
                        'body' => json_encode($credentials)
                    ]
                );
                if ($response->getStatusCode() === 401) {
                    return $this->render('pages/login.html.twig', [
                        'form' => $form->createView(),
                        'error' => "¡Correo o contraseña incorrecta!"
                    ]);
                } else {
                    $json = $response->toArray();
                    $response = $this->redirectToRoute('index_page');
                    $response->headers->setCookie(new Cookie('token', $json['data']['token']));
                    return $response;
                }
            }

            return $this->render('pages/login.html.twig', [
                'form' => $form->createView()
            ]);
        } catch (ClientExceptionInterface|DecodingExceptionInterface|RedirectionExceptionInterface|ServerExceptionInterface|TransportExceptionInterface $e) {
            return $this->render('pages/error.html.twig', [
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
            ]);
        }
    }

    #[Route('/register', name: 'register_page')]
    public function register(Request $request): Response
    {
        try {
            $cookies = $request->cookies;
            if ($cookies->has('token')) {
                return $this->redirectToRoute('index_page');
            }
            $form = $this->createForm(RegisterFormType::class);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $userData = $form->getData();
                $httpClient = HttpClient::create();
                $response = $httpClient->request(
                    'POST',
                    'http://webserver/api/register',
                    [
                        'headers' => [
                            'Content-Type' => 'application/json'
                        ],
                        'body' => json_encode($userData)
                    ]
                );
                if ($response->getStatusCode() === 403) {
                    return $this->render('pages/login.html.twig', [
                        'form' => $form->createView(),
                        'error' => "El correo electronico esta en uso."
                    ]);
                } else {
                    return $this->redirectToRoute('login_page');
                }
            }

            return $this->render('pages/register.html.twig', [
                'form' => $form->createView()
            ]);
        } catch (TransportExceptionInterface|ClientExceptionInterface|DecodingExceptionInterface|RedirectionExceptionInterface|ServerExceptionInterface $e) {
            return $this->render('pages/error.html.twig', [
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
            ]);
        }
    }

    #[Route('/logout', name: 'logout_page')]
    public function logout(): Response
    {
        try {
            $response = $this->redirectToRoute('index_page');
            $response->headers->clearCookie('token');
            return $response;
        } catch (\Exception $e) {
            return $this->render('pages/error.html.twig', [
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
            ]);
        }
    }
}
