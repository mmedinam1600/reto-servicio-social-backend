<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegisterFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('username', EmailType::class, [
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Correo electrónico'
                ),
                'label' => false
            ])
            ->add('firstName', TextType::class, [
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Nombre'
                ),
                'label' => false
            ])
            ->add('lastName', TextType::class, [
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Apellido'
                ),
                'label' => false
            ])
            ->add('password', PasswordType::class, [
                'label' => false,
                'attr' => [
                    'autocomplete' => 'password',
                    'class' => 'form-control',
                    'placeholder' => 'Contraseña'
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Por favor, ingresa una contraseña',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'La contraseña debe tener al menos {{ limit }} caracteres',
                        'max' => 4096
                    ])
                ]
            ])
            ->add('country', TextType::class, [
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'País'
                ),
                'label' => false
            ])
            ->add('postalCode', TextType::class, [
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Código Postal'
                ),
                'label' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
    }
}
