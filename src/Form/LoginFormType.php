<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class LoginFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('username', EmailType::class, [
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Correo electrónico'
                ),
                'label' => false
            ])
            ->add('password', PasswordType::class, [
                'label' => false,
                'attr' => [
                    'autocomplete' => 'password',
                    'class' => 'form-control',
                    'placeholder' => 'Contraseña'
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Por favor, ingresa una contraseña',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'La contraseña debe tener al menos {{ limit }} caracteres',
                        'max' => 4096
                    ])
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {

    }
}
