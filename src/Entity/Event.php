<?php

namespace App\Entity;

use App\Repository\EventRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: EventRepository::class)]
class Event implements JsonSerializable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column()]
    private ?int $id = null;

    #[ORM\Column(length: 1024)]
    #[Assert\Length(max: 1024)]
    #[Assert\NotBlank]
    private ?string $title = null;

    #[ORM\Column(length: 255)]
    #[Assert\Length(max: 255)]
    #[Assert\NotBlank]
    private ?string $organizer = null;

    #[ORM\Column(length: 2048)]
    #[Assert\Length(max: 2048)]
    #[Assert\NotBlank]
    private ?string $description = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Assert\NotBlank]
    private ?\DateTimeInterface $presale = null;

    #[ORM\Column]
    #[Assert\GreaterThanOrEqual(value: 0)]
    #[Assert\NotBlank]
    private ?float $price = null;

    #[ORM\Column(nullable: true)]
    #[Assert\GreaterThanOrEqual(value: 0)]
    private ?int $percentageDiscount = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Assert\NotBlank]
    private ?\DateTimeInterface $scheduledAt = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $restrictions = null;

    #[ORM\Column(length: 255)]
    #[Assert\Length(max: 255)]
    #[Assert\NotBlank]
    private ?string $estimatedDuration = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Assert\Length(max: 255)]
    private ?string $banner = null;

    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    #[Assert\GreaterThanOrEqual(value: 0)]
    private ?int $ageLimit = null;

    #[ORM\Column(nullable: true, options: ["default" => true])]
    private ?bool $active = true;

    #[ORM\ManyToOne(inversedBy: 'events')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $userId = null;

    #[ORM\ManyToOne(inversedBy: 'events')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Venue $venueId = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getOrganizer(): ?string
    {
        return $this->organizer;
    }

    public function setOrganizer(string $organizer): self
    {
        $this->organizer = $organizer;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPresale(): ?\DateTimeInterface
    {
        return $this->presale;
    }

    public function setPresale(?\DateTimeInterface $presale): self
    {
        $this->presale = $presale;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPercentageDiscount(): ?int
    {
        return $this->percentageDiscount;
    }

    public function setPercentageDiscount(?int $percentageDiscount): self
    {
        $this->percentageDiscount = $percentageDiscount;

        return $this;
    }

    public function getScheduledAt(): ?\DateTimeInterface
    {
        return $this->scheduledAt;
    }

    public function setScheduledAt(\DateTimeInterface $scheduledAt): self
    {
        $this->scheduledAt = $scheduledAt;

        return $this;
    }

    public function getRestrictions(): ?string
    {
        return $this->restrictions;
    }

    public function setRestrictions(?string $restrictions): self
    {
        $this->restrictions = $restrictions;

        return $this;
    }

    public function getEstimatedDuration(): ?string
    {
        return $this->estimatedDuration;
    }

    public function setEstimatedDuration(string $estimatedDuration): self
    {
        $this->estimatedDuration = $estimatedDuration;

        return $this;
    }

    public function getBanner(): ?string
    {
        return $this->banner;
    }

    public function setBanner(?string $banner): self
    {
        $this->banner = $banner;

        return $this;
    }

    public function getAgeLimit(): ?int
    {
        return $this->ageLimit;
    }

    public function setAgeLimit(?int $ageLimit): self
    {
        $this->ageLimit = $ageLimit;

        return $this;
    }

    public function isActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getUserId(): ?User
    {
        return $this->userId;
    }

    public function setUserId(?User $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getVenueId(): ?Venue
    {
        return $this->venueId;
    }

    public function setVenueId(?Venue $venueId): self
    {
        $this->venueId = $venueId;

        return $this;
    }

    public function jsonSerialize(): array
    {
        return array(
            "id" => $this->getId(),
            "title" => $this->getTitle(),
            "description" => $this->getDescription(),
            "organizer" => $this->getOrganizer(),
            "price" => $this->getPrice(),
            "percentageDiscount" => $this->getPercentageDiscount(),
            "presale" => $this->getPresale(),
            "scheduledAt" => $this->getScheduledAt(),
            "restrictions" => $this->getRestrictions(),
            "estimatedDuration" => $this->getEstimatedDuration(),
            "banner" => $this->getBanner(),
            "ageLimit" => $this->getAgeLimit(),
            "createdBy" => array(
                "id" => $this->getUserId()->getId(),
                "email" => $this->getUserId()->getEmail(),
                "firstName" => $this->getUserId()->getFirstName(),
                "lastName" => $this->getUserId()->getLastName(),
                "roles" => $this->getUserId()->getRoles(),
            ),
            "venue" => $this->getVenueId(),
        );
    }
}
