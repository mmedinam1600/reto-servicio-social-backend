<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {

        $admin = new User();
        $admin->setFirstName("Administrador");
        $admin->setLastName("AdminPage");
        $admin->setCountry("México");
        $admin->setPostalCode("01245");
        $admin->setEmail("admin@eventos.com");
        $admin->setRoles(["ROLE_USER", "ROLE_ADMIN"]);
        $admin->setPassword("$2y$13\$Y8SWvA9dOHUsgE2RodmqWO6YagmMPlYa2Gw0cVan2iOIRHj3MzpYK");
        $manager->persist($admin);

        $user = new User();
        $user->setFirstName("Miguel");
        $user->setLastName("Medina");
        $user->setCountry("México");
        $user->setPostalCode("01459");
        $user->setEmail("miguel@gmail.com");
        $user->setRoles(["ROLE_USER"]);
        $user->setPassword("$2y$13\$Y8SWvA9dOHUsgE2RodmqWO6YagmMPlYa2Gw0cVan2iOIRHj3MzpYK");
        $manager->persist($user);

        $manager->flush();

        $this->addReference('admin', $admin);
        $this->addReference('user', $user);
    }
}
