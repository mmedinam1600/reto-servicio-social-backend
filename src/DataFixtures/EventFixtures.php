<?php


namespace App\DataFixtures;

use App\Entity\Event;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class EventFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $event = new Event();
        $event->setTitle("Harry Styles: Love on Tour | Premium Seat VIP");
        $event->setDescription("Su asiento Premium VIP incluye los siguientes servicios:
        - Un billete de asiento en la mejor categoría
        - Regalo de mercancía exclusivo para los compradores de paquetes
        - Pase conmemorativo y cordón de seguridad
        - Persona de contacto in situ y registro independiente");
        $event->setAgeLimit(3);
        $event->setBanner("https://s1.ticketm.net/img/tat/dam/a/f43/10a0ba85-3be9-40b7-9d05-07053aa78f43_1595521_CUSTOM.jpg");
        $event->setEstimatedDuration("2 horas.");
        $event->setOrganizer("Harry Styles");
        $event->setPercentageDiscount(10);
        $event->setPrice(7000);
        $event->setPresale(new \DateTime("10/25/2022 14:30:00"));
        $event->setScheduledAt(new \DateTime("10/30/2022 14:30:00"));
        $event->setRestrictions("Se puede pedir un máximo de 10 entradas por persona para este evento.");
        $event->setUserId($this->getReference('user'));
        $event->setVenueId($this->getReference('venue1'));
        $manager->persist($event);

        $event2 = new Event();
        $event2->setTitle("Harry Styles | Premium Seat VIP");
        $event2->setDescription("Su asiento Premium VIP incluye los siguientes servicios:
        - Un billete de asiento en la mejor categoría
        - Regalo de mercancía exclusivo para los compradores de paquetes
        - Pase conmemorativo y cordón de seguridad
        - Persona de contacto in situ y registro separado");
        $event2->setAgeLimit(3);
        $event2->setBanner("https://s1.ticketm.net/img/tat/dam/a/f43/10a0ba85-3be9-40b7-9d05-07053aa78f43_1595521_CUSTOM.jpg");
        $event2->setEstimatedDuration("2 horas.");
        $event2->setOrganizer("Harry Styles");
        $event2->setPercentageDiscount(null);
        $event2->setPrice(5000);
        $event2->setPresale(new \DateTime("06/22/2022 18:00:00"));
        $event2->setScheduledAt(new \DateTime("06/30/2022 18:00:00"));
        $event2->setRestrictions("Se puede pedir un máximo de 10 entradas por persona para este evento.");
        $event2->setUserId($this->getReference('user'));
        $event2->setVenueId($this->getReference('venue2'));
        $manager->persist($event2);

        $event3 = new Event();
        $event3->setTitle("HARRY STYLES - Love on Tour");
        $event3->setDescription("Su asiento Premium VIP incluye los siguientes servicios:
        - Un billete de asiento en la mejor categoría
        - Regalo de mercancía exclusivo para los compradores de paquetes
        - Pase conmemorativo y cordón de seguridad
        - Persona de contacto in situ y registro separado");
        $event3->setAgeLimit(3);
        $event3->setBanner("https://s1.ticketm.net/img/tat/dam/a/f43/10a0ba85-3be9-40b7-9d05-07053aa78f43_1595521_CUSTOM.jpg");
        $event3->setEstimatedDuration("1 hora 30 minutos.");
        $event3->setOrganizer("Harry Styles");
        $event3->setPercentageDiscount(null);
        $event3->setPrice(3500);
        $event3->setPresale(new \DateTime("08/15/2022 15:00:00"));
        $event3->setScheduledAt(new \DateTime("08/30/2022 15:00:00"));
        $event3->setRestrictions("Las comisiones y los posibles gastos de envío se aplicarán a su pedido. Límite de entradas compradas: 4");
        $event3->setUserId($this->getReference('user'));
        $event3->setVenueId($this->getReference('venue3'));
        $manager->persist($event3);

        $event4 = new Event();
        $event4->setTitle("Harry Styles - Love On Tour");
        $event4->setDescription("Harry Styles traerá ‘Love On Tour’ el 18 de mayo al WiZink Center de Madrid. El artista británico recorrerá toda Europa y Norteamérica presentando su nuevo trabajo ‘Fine Line’, el segundo de su carrera en solitario tras su anterior etapa en One Direction.");
        $event4->setAgeLimit(16);
        $event4->setBanner("https://s1.ticketm.net/img/tat/dam/a/b3e/0856071a-d637-4c62-a706-b133a327cb3e_1595031_CUSTOM.jpg");
        $event4->setEstimatedDuration("2 horas 30 minutos.");
        $event4->setOrganizer("Harry Styles");
        $event4->setPercentageDiscount(null);
        $event4->setPrice(3500);
        $event4->setScheduledAt(new \DateTime("09/29/2022 21:00:00"));
        $event4->setRestrictions("Las comisiones y los posibles gastos de envío se aplicarán a su pedido. Límite de entradas compradas: 4");
        $event4->setUserId($this->getReference('user'));
        $event4->setVenueId($this->getReference('venue4'));
        $manager->persist($event4);


        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [UserFixtures::class, VenueFixtures::class];
    }
}
