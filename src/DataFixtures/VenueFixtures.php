<?php

namespace App\DataFixtures;

use App\Entity\Venue;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class VenueFixtures extends Fixture
{

    public function load(ObjectManager $manager): void
    {
        $venue = new Venue();
        $venue->setName("Mercedes-Benz Arena");
        $venue->setPostalCode("10243");
        $venue->setCountry("Germany");
        $venue->setCity("Berlin");
        $venue->setAddress("Mercedes-Platz 1, 10243 Berlin, Germany");
        $manager->persist($venue);

        $venue2 = new Venue();
        $venue2->setName("LANXESS arena - Cologne");
        $venue2->setPostalCode("50679");
        $venue2->setCountry("Germany");
        $venue2->setCity("Cologne");
        $venue2->setAddress("Willy Brandt Platz 1");
        $manager->persist($venue2);

        $venue3 = new Venue();
        $venue3->setName("Unipol Arena");
        $venue3->setPostalCode("40033");
        $venue3->setCountry("Italy");
        $venue3->setCity("Province of Bologna");
        $venue3->setAddress("Via Gino Cervi, 2, 40033 Casalecchio di Reno BO, Italy");
        $manager->persist($venue3);

        $venue4 = new Venue();
        $venue4->setName("WiZink Center");
        $venue4->setPostalCode("28009");
        $venue4->setCountry("Spain");
        $venue4->setCity("Madrid");
        $venue4->setAddress("Av. Felipe II, s/n, 28009 Madrid, Spain");
        $manager->persist($venue4);

        $manager->flush();

        $this->addReference('venue1', $venue);
        $this->addReference('venue2', $venue2);
        $this->addReference('venue3', $venue3);
        $this->addReference('venue4', $venue4);
    }
}
