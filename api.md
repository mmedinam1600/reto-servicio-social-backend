# Documentación de la API

<details>
  <summary>Tabla de contenidos</summary>
  <ol>
    <li>
      <a href="#endpoints-públicos">Endpoints públicos</a>
      <ul>
        <li><a href="#registro">Registro</a></li>
        <ul>
            <li><a href="#respuesta-exitosa">Respuesta exitosa</a></li>
            <li><a href="#respuestas-erróneas">Respuestas erróneas</a></li>
        </ul>
        <li><a href="#inicio-de-sesión">Inicio de sesión</a></li>
        <ul>
            <li><a href="#respuesta-exitosa">Respuesta exitosa</a></li>
            <li><a href="#respuestas-erróneas">Respuestas erróneas</a></li>
        </ul>
        <li><a href="#obtener-eventos">Obtener eventos</a></li>
        <ul>
            <li><a href="#respuesta-exitosa">Respuesta exitosa</a></li>
            <li><a href="#respuestas-erróneas">Respuestas erróneas</a></li>
        </ul>
        <li><a href="#obtener-evento-por-id">Obtener evento por ID</a></li>
        <ul>
            <li><a href="#respuesta-exitosa">Respuesta exitosa</a></li>
            <li><a href="#respuestas-erróneas">Respuestas erróneas</a></li>
        </ul>
      </ul>
    </li>
    <li>
      <a href="#endpoints-privados">Endpoints privados</a>
      <ul>
        <li><a href="#crear-nuevo-evento">Crear nuevo evento</a></li>
        <ul>
            <li><a href="#respuesta-exitosa">Respuesta exitosa</a></li>
            <li><a href="#respuestas-erróneas">Respuestas erróneas</a></li>
        </ul>
        <li><a href="#editar-evento">Editar evento</a></li>
        <ul>
            <li><a href="#respuesta-exitosa">Respuesta exitosa</a></li>
            <li><a href="#respuestas-erróneas">Respuestas erróneas</a></li>
        </ul>
        <li><a href="#remover-evento">Remover evento</a></li>
        <ul>
            <li><a href="#respuesta-exitosa">Respuesta exitosa</a></li>
        </ul>
      </ul>
    </li>
  </ol>
</details>

## Endpoints públicos

Los endpoints públicos no requieren de autentificación.

### Registro

Registra una nueva cuenta en la API

**URL** : `/api/register`

**Método** : `POST`

**Autentificación requerida** : `NO`

**Permisos requeridos** : `Ninguno`

**JSON Data** :

```json
{
    "username": "[email]",
    "firstName": "[nombre]",
    "lastName": "[apellido]",
    "password": "[contraseña]",
    "country": "[País]",
    "postalCode": "[Código postal]"
}
```

**Data example** :

```json
{
    "username": "test@gmail.com",
    "firstName": "Test",
    "lastName": "Test",
    "password": "password",
    "country": "Canada",
    "postalCode": "01420"
}
```

#### Respuesta exitosa

**Condición** : Si todo es OK y el correo no está registrado.

**Code** : `201`

**Content example**

```json
{
    "message": "Usuario creado satisfactoriamente",
    "data": {}
}
```

#### Respuestas erróneas

**Condición** : Si el correo ya esta registrado.

**Code** : 400

**Content example**

```json
{
    "message": "Correo electrónico en uso.",
    "data": {},
    "errors": [
        {
            "message": "Este correo ya esta registrado.",
            "statusCode": 400
        }
    ]
}
```

### Inicio de sesión

Inicia sesión con una cuenta en la API

**URL** : `/api/login`

**Método** : `POST`

**Autentificación requerida** : `NO`

**Permisos requeridos** : `Ninguno`

**JSON Data** :

```json
{
    "username": "[email]",
    "password": "[contraseña]"
}
```

**Data example** :

```json
{
    "username": "test@gmail.com",
    "password": "password"
}
```

#### Respuesta exitosa

**Condición** : Si todo es OK y el correo no está registrado.

**Code** : `200`

**Content example**

```json
{
    "data": {
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE2NTg5MzI1NjEsImV4cCI6MTY2NDExNjU2MSwicm9sZXMiOlsiUk9MRV9VU0VSIl0sInVzZXJuYW1lIjoidGVzdEBnbWFpbC5jb20iLCJpcCI6IjE3Mi4xOS4wLjEifQ.Leat1gHmaIMPsCft5pf38C4lIuYoZgL6qS2B8qxyJJcI8MND1QD9TyZ5Fb180C9dZOHMLTFMRBHzz48YS5dKFUJNaHDTSUVj7qPlkDUTD6lWNY2iGtcTMv7VvyEmNyBn61EWPtLpf82bj3eSNoZ__y02Y_5IgEZ49RLSioZtq7i8p8J6kLfKhoRH9LXWloWOZ_0ORDkjjyCaBeFMQ003uxI-RZIBmyGx7Feu-Q2gpBvY1tIZStEIfT7QKQSh4BASw1xc98ndUPBqmIWq1-bp1pLxjuj-zOTRo8BYQiGCR-7rX6BKY08o0uetaC-PPBEtvdU18H-aulGhcgsZ_tKBqg"
    }
}
```

#### Respuestas erróneas

**Condición** : Si el correo o la contraseña no coinciden.

**Code** : 401

**Content example**

```json
{
    "error": {
        "message": "Bad credentials, please verify that your username/password are correctly set",
        "statusCode": 400
    }
}
```

### Obtener eventos

Obtiene los eventos registrados de la API.

**URL** : `/api/events?`

**Parámetros
URL** : `page=[integer | default: 1]` `limit=[integer | default: 10]` `category=[0 | 1 | 2 | 3 | default: 0]`

**Método** : `GET`

**Autentificación requerida** : `NO`

**Permisos requeridos** : `Ninguno`

**JSON Data** :

```json
{}
```

**Data example** :

```json
{}
```

#### Respuesta exitosa

**Condición** : Si los parámetros de la URL son números válidos.

**Code** : `200`

**Content example**

```json
{
    "message": "Listado de eventos",
    "data": [
        {
            "id": 34,
            "title": "Harry Styles | Premium Seat VIP",
            "description": "Su asiento Premium VIP incluye los siguientes servicios:\n        - Un billete de asiento en la mejor categoría\n        - Regalo de mercancía exclusivo para los compradores de paquetes\n        - Pase conmemorativo y cordón de seguridad\n        - Persona de contacto in situ y registro separado",
            "organizer": "Harry Styles",
            "price": 5000,
            "percentageDiscount": null,
            "presale": {
                "date": "2022-06-22 18:00:00.000000",
                "timezone_type": 3,
                "timezone": "UTC"
            },
            "scheduledAt": {
                "date": "2022-06-30 18:00:00.000000",
                "timezone_type": 3,
                "timezone": "UTC"
            },
            "restrictions": "Se puede pedir un máximo de 10 entradas por persona para este evento.",
            "estimatedDuration": "2 horas.",
            "banner": "https://s1.ticketm.net/img/tat/dam/a/f43/10a0ba85-3be9-40b7-9d05-07053aa78f43_1595521_CUSTOM.jpg",
            "ageLimit": 3,
            "createdBy": {
                "id": 22,
                "email": "miguel@gmail.com",
                "firstName": "Miguel",
                "lastName": "Medina",
                "roles": [
                    "ROLE_USER"
                ]
            },
            "venue": {
                "id": 34,
                "name": "LANXESS arena - Cologne",
                "address": "Willy Brandt Platz 1",
                "country": "Germany",
                "city": "Cologne",
                "postalCode": "50679"
            }
        }
    ]
}
```

#### Respuestas erróneas

**Condición** : Si algunos argumentos son inválidos como números negativos.

**Code** : 500

**Content example**

```json
{
    "message": "Error al obtener los eventos",
    "data": {},
    "errors": [
        {
            "message": "Offset must be a positive integer or zero, -22 given",
            "statusCode": 500
        }
    ]
}
```

### Obtener evento por ID

Obtiene un evento por ID.

**URL** : `/api/events/:id/`

**Parámetros
URL** : `id=[integer]`

**Método** : `GET`

**Autentificación requerida** : `NO`

**Permisos requeridos** : `Ninguno`

**JSON Data** :

```json
{}
```

**Data example** :

```json
{}
```

#### Respuesta exitosa

**Condición** : Si el ID del evento es válido.

**Code** : `200`

**Content example**

```json
{
    "message": "Evento obtenido",
    "data": {
        "id": 35,
        "title": "HARRY STYLES - Love on Tour",
        "description": "Su asiento Premium VIP incluye los siguientes servicios:\n        - Un billete de asiento en la mejor categoría\n        - Regalo de mercancía exclusivo para los compradores de paquetes\n        - Pase conmemorativo y cordón de seguridad\n        - Persona de contacto in situ y registro separado",
        "organizer": "Harry Styles",
        "price": 3500,
        "percentageDiscount": null,
        "presale": {
            "date": "2022-08-15 15:00:00.000000",
            "timezone_type": 3,
            "timezone": "UTC"
        },
        "scheduledAt": {
            "date": "2022-08-30 15:00:00.000000",
            "timezone_type": 3,
            "timezone": "UTC"
        },
        "restrictions": "Las comisiones y los posibles gastos de envío se aplicarán a su pedido. Límite de entradas compradas: 4",
        "estimatedDuration": "1 hora 30 minutos.",
        "banner": "https://s1.ticketm.net/img/tat/dam/a/f43/10a0ba85-3be9-40b7-9d05-07053aa78f43_1595521_CUSTOM.jpg",
        "ageLimit": 3,
        "createdBy": {
            "id": 22,
            "email": "miguel@gmail.com",
            "firstName": "Miguel",
            "lastName": "Medina",
            "roles": [
                "ROLE_USER"
            ]
        },
        "venue": {
            "id": 35,
            "name": "Unipol Arena",
            "address": "Via Gino Cervi, 2, 40033 Casalecchio di Reno BO, Italy",
            "country": "Italy",
            "city": "Province of Bologna",
            "postalCode": "40033"
        }
    }
}
```

#### Respuestas erróneas

**Condición** : Si el id del evento no existe

**Code** : 400

**Content example**

```json
{
    "message": "El evento con el id 350 no existe",
    "data": {},
    "errors": [
        {
            "message": "Error al obtener el evento con el id 350",
            "statusCode": 400
        }
    ]
}
```

## Endpoints privados

Los endpoints privados requieren de un token de autentificación generado por el
endpoint de inicio de sesión.

### Crear nuevo evento

Crea un nuevo evento.

**URL** : `/api/events/`

**Método** : `POST`

**Autentificación requerida** : `SI`

**Permisos requeridos** : `ROLE_USER`

**JSON Data** :

```json
{
    "title": "[título]",
    "organizer": "[organizador]",
    "description": "[descripción]",
    "presale": "[preventa]",
    "price": 0,
    "percentageDiscount": 0,
    "scheduledAt": "[programado]",
    "restrictions": "[restricciones]",
    "estimatedDuration": "[duración estimada]",
    "banner": "[banner url]",
    "ageLimit": 0,
    "venueId": 0
}
```

> NOTA: DE momento la única manera de saber el ID de la sede es revisando la base de datos.

**Data example** :

```json
{
    "title": "Evento de FinTech",
    "organizer": "Elon Musk",
    "description": "Evento de tecnología y finanzas",
    "presale": "2023/01/01 10:30:00",
    "price": 3446.0,
    "percentageDiscount": 57.0,
    "scheduledAt": "2023/01/05 10:30:00",
    "restrictions": "Llevar identificación oficial (INE o pasaporte)",
    "estimatedDuration": "2 horas",
    "banner": "https://xegmenta.com/wp-content/uploads/2019/06/organizar-evento-corp-opt.jpg",
    "ageLimit": 18.0,
    "venueId": 34.0
}
```

#### Respuesta exitosa

**Condición** : Si todos los parámetros son válidos

**Code** : `201`

**Content example**

```json
{
    "message": "Evento creado satisfactoriamente",
    "data": {
        "id": 54,
        "title": "Evento de FinTech",
        "description": "Evento de tecnología y finanzas",
        "organizer": "Elon Musk",
        "price": 3446,
        "percentageDiscount": 57,
        "presale": {
            "date": "2023-01-01 10:30:00.000000",
            "timezone_type": 3,
            "timezone": "UTC"
        },
        "scheduledAt": {
            "date": "2023-01-05 10:30:00.000000",
            "timezone_type": 3,
            "timezone": "UTC"
        },
        "restrictions": "Llevar identificación oficial (INE o pasaporte)",
        "estimatedDuration": "2 horas",
        "banner": "https://xegmenta.com/wp-content/uploads/2019/06/organizar-evento-corp-opt.jpg",
        "ageLimit": 18,
        "createdBy": {
            "id": 32,
            "email": "test@gmail.com",
            "firstName": "Test",
            "lastName": "Test",
            "roles": [
                "ROLE_USER"
            ]
        },
        "venue": {
            "id": 34,
            "name": "LANXESS arena - Cologne",
            "address": "Willy Brandt Platz 1",
            "country": "Germany",
            "city": "Cologne",
            "postalCode": "50679"
        }
    }
}
```

#### Respuestas erróneas

**Condición** : Si algún parámetro no existe o no coincide con el valor.

**Code** : 500

**Content example**

```json
{
    "message": "Error general",
    "data": {},
    "errors": [
        "Warning: Undefined array key \"title\""
    ]
}
```

### Editar evento

Edita un evento.

**URL** : `/api/events/:id`

**Parámetros URL** : `id=[number]`

**Método** : `PUT`

**Autentificación requerida** : `SI`

**Permisos requeridos** : `ROLE_ADMIN`

**JSON Data** :

```json
{
    "title": "[título]",
    "organizer": "[organizador]",
    "description": "[descripción]",
    "presale": "[preventa]",
    "price": 0,
    "percentageDiscount": 0,
    "scheduledAt": "[programado]",
    "restrictions": "[restricciones]",
    "estimatedDuration": "[duración estimada]",
    "banner": "[banner url]",
    "ageLimit": 0,
    "venueId": 0
}
```

> NOTA: DE momento la única manera de saber el ID de la sede es revisando la base de datos.

**Data example** :

```json
{
    "title": "Evento de FinTech Editado",
    "organizer": "Elon Musk",
    "description": "Evento de tecnología y finanzas",
    "presale": "2023/01/01 10:30:00",
    "price": 3446.0,
    "percentageDiscount": 57.0,
    "scheduledAt": "2023/01/05 10:30:00",
    "restrictions": "Llevar identificación oficial (INE o pasaporte)",
    "estimatedDuration": "2 horas",
    "banner": "https://xegmenta.com/wp-content/uploads/2019/06/organizar-evento-corp-opt.jpg",
    "ageLimit": 18.0,
    "venueId": 54.0
}
```

#### Respuesta exitosa

**Condición** : Si todos los parámetros son válidos

**Code** : `201`

**Content example**

```json
{
    "message": "Evento editado satisfactoriamente",
    "data": {
        "id": 54,
        "title": "Evento de FinTech Editado",
        "description": "Evento de tecnología y finanzas",
        "organizer": "Elon Musk",
        "price": 3446,
        "percentageDiscount": 57,
        "presale": {
            "date": "2023-01-01 10:30:00.000000",
            "timezone_type": 3,
            "timezone": "UTC"
        },
        "scheduledAt": {
            "date": "2023-01-05 10:30:00.000000",
            "timezone_type": 3,
            "timezone": "UTC"
        },
        "restrictions": "Llevar identificación oficial (INE o pasaporte)",
        "estimatedDuration": "2 horas",
        "banner": "https://xegmenta.com/wp-content/uploads/2019/06/organizar-evento-corp-opt.jpg",
        "ageLimit": 18,
        "createdBy": {
            "id": 32,
            "email": "test@gmail.com",
            "firstName": "Test",
            "lastName": "Test",
            "roles": [
                "ROLE_USER"
            ]
        },
        "venue": {
            "id": 34,
            "name": "LANXESS arena - Cologne",
            "address": "Willy Brandt Platz 1",
            "country": "Germany",
            "city": "Cologne",
            "postalCode": "50679"
        }
    }
}
```

#### Respuestas erróneas

**Condición** : Si algún parámetro no existe o no coincide con el valor.

**Code** : 403

**Content example**

```json
{
    "message": "Error general",
    "data": {},
    "errors": [
        "Warning: Undefined array key \"title\""
    ]
}
```

### Remover evento

Edita un evento.

**URL** : `/api/events/:id`

**Parámetros URL** : `id=[number]`

**Método** : `DELETE`

**Autentificación requerida** : `SI`

**Permisos requeridos** : `ROLE_ADMIN`

**JSON Data** :

```json
{}
```

**Data example** :

```json
{}
```

#### Respuesta exitosa

**Condición** : Si el id del evento existe

**Code** : `200`

**Content example**

```json
{
    "message": "Evento con id: 38 borrado satisfactoriamente",
    "data": {}
}
```
